import scrapy
from scrapy.http import Request
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
import sys

try:
    from urllib.parse import urlparse, urljoin
except ImportError:
     from urlparse import urlparse, urljoin

# run: scrapy runspider LehrerwebSpider.py

# based upon  https://stackoverflow.com/a/37250823/2493671
class LehrerwebSpider(CrawlSpider):
    name = 'Lehrerweb'
    start_urls = ['https://legacy.lehrerweb.at/']
    allowed_domains = [
        'legacy.lehrerweb.at'
    ]
    ignore_urls = [
        '/fileadmin/',
        'javascript:',
        '#'
    ]
    DOWNLOAD_HANDLERS = {
    'https': 'my.custom.downloader.handler.https.HttpsDownloaderIgnoreCNError'
    }
    rules = (
        Rule(LinkExtractor(), callback='parse_item', follow=True),
    )
    printed_urls = []

    def parse_item(self, response):
        try:
             for url in response.xpath('//a/@href').extract():
                # if url is not empty, is not in visitedUrls and does not start with a value from ignore_urls
                if url and url not in self.printed_urls and list(filter(url.startswith, self.ignore_urls)) == []:

                    parse_result=urlparse(url)
                    domain=parse_result.netloc

                    if domain: # absolute url
                        if domain in self.allowed_domains:
                            print(url)
                        else: # print debug message, when the url is not allowed
                            self.logger.debug(['domain not allowed', response.status, domain])
                    else: # relative url
                        print(url)


                    self.printed_urls.append(url)
                    scrapy.http.Request(urljoin(response.url, url))
        except:
            e = sys.exc_info()[0]
            self.logger.debug(e)
            self.logger.debug(['error', response.status, response.url])
