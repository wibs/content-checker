import operator
import functools

# taken from https://www.geeksforgeeks.org/python-get-unique-values-list/
def unique(list_):
    """ Return only unique values from a list. """
    # intilize a null list
    unique_list = []

    # traverse for all elements
    for x in list_:
        # check if exists in unique_list or not
        if x not in unique_list:
            unique_list.append(x)

    return unique_list

# based upon https://stackoverflow.com/a/952952/2493671
def flatten(list_):
    """ Flatten a multi-demnsional list. """
    return functools.reduce(operator.iconcat, list_, [])
