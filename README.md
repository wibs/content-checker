# README

## Requirments

+ Python3
+ `requests`, `bs4` (BeautifulSoup), `scrapy`

## Run Spider

```shell
$ pip install scrapy # installs scrapy (you need Python for this)
$ scrapy runspider LehrerwebSpider.py > >(tee results.txt) 2> >(tee stderr.log >&2) # runs spider and prints all urls to result.txt - PYTHON 2!!
```

A result list compiled by this spider can be found in `spiders/result.txt` - this list is not necessarily up to date, though.

The Spider gives you a list of all pages on Lehrerweb. You can use the `LehrerwebImageCheck.py` script without any params to compile a list of paths that contain errors within the image caption.

```shell
$ python3 LehrerwebImageCheck.py
```

The following checks are run:
+ Do the number of image caption items on the legacy page equal the number on the prod page? No: Bug, Yes:
Continue
+ Do the legacy caption items exist on the prod page? Yes: all good, No: Continue
+ Is there an item on the prod page that has the same text content as the items on the legacy page? Yes: all good, No: Continue (this also checks for `href` Attribute on `<a>`-Tags)

If none of this checks passes, an error is logged to the console.

Please feel free to fix bugs and add functionality as needed.
