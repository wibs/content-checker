#!/usr/bin/python3
# -*- coding: UTF-8 -*-

###
# THIS IS NOT THOROUGHLY TESTED YET AND MIGHT RESULT IN WRONG OUTPUT!
###
import requests
from bs4 import BeautifulSoup
import urllib3
import functools
import operator
import helpers
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

paths = open('./spiders/results.txt', 'r')

prod_base = 'https://lehrerweb.wien'
legacy_base = 'https://legacy.lehrerweb.at'

# loop through resultst.txt
for line in helpers.unique(paths):
    _line = line.strip()
    legacy_url = legacy_base + _line
    prod_url = prod_base + _line

    # load html
    legacy_html = requests.get(legacy_url, verify=False, allow_redirects=True, headers={'User-Agent' : 'Mozilla/5.0'}).text
    prod_html = requests.get(prod_url, allow_redirects=True, headers={'User-Agent' : 'Mozilla/5.0'}).text

    # parse html
    legacy_soup = BeautifulSoup(legacy_html, features="lxml")
    prod_soup = BeautifulSoup(prod_html, features="lxml")

    # find all <a> elements
    legacy_hits = legacy_soup.find_all('a')
    prod_hits = legacy_soup.find_all('a')

    # remove duplicates from hits
    legacy_hits = helpers.unique(legacy_hits)
    prod_hits = helpers.unique(prod_hits)

    # Compare legacy_hits to prod_hits
    if len(legacy_hits) != len(prod_hits):
        print('✗', {'path': _line, 'error': 'number of hits does not equal', 'legacy_hits': len(legacy_hits), 'prod_hits': len(prod_hits)})

    for lhit in legacy_hits:
        if lhit in prod_hits: # lhit is in prod_hits
            print('✓', {'path': _line, 'lhit': lhit} )
            continue

        for phit in prod_hits:
            if lhit.text.strip() == phit.text.strip() and lhit['href'] == phit['href']: # hittext and hrefs equal
                print('✓', {'path': _line, 'lhit': lhit, 'phit': phit} )
            else:
                print('✗', {'path': _line, 'error': 'lhit does not equal any phit', 'lhit': lhit, 'phit': phit})
