import { pagesWithImages } from '../../../pages'

/**
 * based upon
 * https://stackoverflow.com/questions/57529877/cypress-comparing-information-in-2-sites
 */
describe('Lehrerweb: Image captions', () => {

  const _selectors = [
    '.copyright__item',
    '.ce-gallery__figcaption',
  ]
  const selectors = _selectors.join(', ')

  // overrideUrls is here to quickly test one single url and not all of them at
  // once
  const overrideUrls = []
  const urls = overrideUrls.length > 0 ? overrideUrls : pagesWithImages

  // Loop through all pages with images
  urls.forEach((url) => {

    // Prepare stuff
    const prodUrl = 'https://lehrerweb.wien' + url
    const legacyUrl = 'https://legacy.lehrerweb.at' + url
    // Will contain items to check for on the prodUrl
    const $captionNodes = []

    it('path: ' + url, () => {

      // Intercept Google Analytics, because we do not need to track those
      // requests AND we do not need to load those files for performance
      // reasons.
      cy.intercept('POST', 'https://www.google-analytics.com/j/collect',
        { statusCode: 200 }).as('collect')
      cy.intercept('GET', 'https://www.google-analytics.com/collect',
        { statusCode: 200 }).as('gifCollect')

      // Visit legacyUrl
      cy.visit(legacyUrl)

      // Return, when the selectors above do not return any nodes
      if (Cypress.$(selectors).length <= 0) {
        cy.debug('no caption items found on this page.')
        return null
      }

      // Get all caption items from legacyUrl and compare them to the prodUrl
      cy.get(selectors).each(($node, index) => {
        $captionNodes.push($node)
      }).then(() => {
        expect(selectors).length.to.be.greaterThan(0)
        if ($captionNodes.length > 0) {
          expect($captionNodes).length.to.be.greaterThan(0)

          cy.request('GET', prodUrl).should(({ body, status }) => {
            expect(status).to.equal(200)

            // Get all nodes from the prod url
            const _items = Cypress.$(body).find(selectors).toArray()

            $captionNodes.map(($item, index) => {
              /**
               * Get values from legacy.lehrerweb.at
               */
              const contentOfItem = $item[0].textContent.trim()
              const hrefOfItem = $item[0].href

              /**
               * Compare to values from lehrerweb.wien
               */
              const _item = _items[index]
              expect(_item).to.contain.text(contentOfItem)

              // Compare url only if it is set
              if (hrefOfItem !== undefined) {
                expect(_item).to.have.attr('href', hrefOfItem)
              }
            })
          })
        }
      })
    })
  })
})
