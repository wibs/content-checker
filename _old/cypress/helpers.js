'use strict'

/**
 * @param className
 * @return string
 */
export const imageCaptionClassName = (className) => {
  return className
    .replace('copyright-info', '')
    .replace('copyright__item', '')
    .trim()
}
